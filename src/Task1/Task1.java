package Task1;

import java.util.Scanner;

public class Task1 {

    // Method to count how many candles on the cake are tallest
    public static int birthdayCakeCandles(int[] candles) {
        int maxHeightOfCandle = 0;
        int numberCandlesOfMaxHeight = 0;
        for (int candleHeight : candles) {
            if (candleHeight == maxHeightOfCandle) {
                numberCandlesOfMaxHeight++;
            }
            if (candleHeight > maxHeightOfCandle) {
                maxHeightOfCandle = candleHeight;
                numberCandlesOfMaxHeight = 1;
            }
        }
        return numberCandlesOfMaxHeight;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        // Input total number of candles on the cake.
        int n = in.nextInt();

        // Check for constraints.
        if (n<1 || n>1000)
            throw new IllegalArgumentException("Attention! This value is not allowed. " +
                    "Number of candles must be in range 1..1000.");

        // Input heights for every candle on the cake.
        int[] candles = new int[n];
        for (int i=0; i<n; i++) {
            candles[i] = in.nextInt();

        // Check for constraints.
            if (candles[i]<1 || candles[i]>1000)
                throw new IllegalArgumentException("Attention! This value is not allowed. " +
                        "Height of candles must be in range 1..1000.");
            }

        // Output the number of the tallest candles on the cake.
        System.out.println("Nr. of the tallest candles on the cake: " + birthdayCakeCandles(candles));
    }
}