package Task2;

import java.io.*;
import java.util.*;

public class TaskManager {

    ArrayList<User> users = new ArrayList<>();

    public void showAllUsersWithoutTasks() throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("users.ser");
            ObjectInputStream in = new ObjectInputStream(fis);
            users = (ArrayList<User>) in.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
        } catch (Exception e) {
            System.out.println("Another IOException.");
            e.printStackTrace();
        } finally {
            if (fis != null)
                fis.close();
        }
        System.out.println("Users list:\n" + users);
    }

    public void saveUsersToFile() throws IOException {
        FileOutputStream fos = new FileOutputStream("users.ser");
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(users) ;
    }

    public void createNewUser() throws IOException {
        BufferedReader stdin = new BufferedReader (new InputStreamReader (System.in));
        while (true) {
            System.out.print("\n First Name: ");
            String firstName = stdin.readLine();
            System.out.print(" Last Name: ");
            String lastName = stdin.readLine();
            System.out.print(" Username: ");
            String userName = stdin.readLine();
            users.add (new User (firstName, lastName, userName));
            System.out.print(" Create one more user? ( Y / N ) " ) ;
            String answer = stdin.readLine().toUpperCase();
            if (answer.startsWith("N"))
                break;
        }
    }

    public static void main (String[] args) throws IOException {
        TaskManager app = new TaskManager();

        // Show users list from the file
        app.showAllUsersWithoutTasks();

        // Create new users
        app.createNewUser();

        // Save new users in the file
        app.saveUsersToFile();
    }
}