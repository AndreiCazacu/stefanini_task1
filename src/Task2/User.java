package Task2;

import java.io.*;

class User implements Serializable {

    public String firstName, lastName, userName;

    public User (String firstName , String lastName , String userName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
    }

    public String toString() {
        return firstName + " " + lastName + " ( " + userName + " )";
    }
}
