package Task2;

import java.io.*;

public class TaskDetails implements Serializable {

    public String taskTitle, description;

    public TaskDetails (String taskTitle , String description) {
        this.taskTitle = taskTitle;
        this.description = description;
    }

    public String toString() {
        return taskTitle + " ( " + description + " )";
    }
}
